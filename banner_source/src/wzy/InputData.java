package wzy;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.apache.commons.configuration.XMLConfiguration;

import banner.eval.BANNER;
import banner.eval.dataset.BioCDataset;
import banner.eval.dataset.Dataset;

public class InputData {

	public void ReadTrainDataFile(HierarchicalConfiguration config)
	{
		Dataset dataset= BANNER.getDataset(config);
		dataset.load(config);
		
	}
	
	
	public static void main(String[] args) throws ConfigurationException
	{
		InputData inputdata=new InputData();
		HierarchicalConfiguration config=new XMLConfiguration("F:\\Workspace\\Banner\\banner_source\\config\\banner_bioc.xml");
		inputdata.ReadTrainDataFile(config);
	}
	
}
