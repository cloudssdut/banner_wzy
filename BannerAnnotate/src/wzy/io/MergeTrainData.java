package wzy.io;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;

public class MergeTrainData {

	
	public String ReadOneLineFile(String filename) throws IOException
	{
		BufferedReader br=new BufferedReader(new FileReader(filename));
		String buffer=br.readLine();
		br.close();
		return buffer;
	}
	
	public void WriteOneBigLine(String filename,String buffer) throws FileNotFoundException
	{
		PrintStream ps=new PrintStream(filename);
		ps.println(buffer);
		ps.close();
	}
	
	public static void main(String[] args) throws IOException
	{
		String dir="F:\\Workspace\\Banner\\banner_source\\data\\";
		String[] files={"expert1_bioc.xml","mturk1_bioc.xml","mturk2_bioc.xml"};
		MergeTrainData mtd=new MergeTrainData();
		StringBuilder sb=new StringBuilder();
		for(int i=0;i<files.length;i++)
		{
			try {
				sb.append(mtd.ReadOneLineFile(dir+files[i]));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		mtd.WriteOneBigLine(dir+"3.xml", sb.toString());
	}
}
