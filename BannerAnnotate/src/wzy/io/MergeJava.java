package wzy.io;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MergeJava {

	public List<String> ReadJavaFile(String filename) throws IOException
	{
		List<String> tuples=new ArrayList<String>();
		BufferedReader br=new BufferedReader(new FileReader(filename));
		String buffer=null;
		while((buffer=br.readLine())!=null)
		{
			if(buffer.length()<2)
				continue;
			String[] ss=buffer.split(",");
			//System.out.println("*"+ss.length);
			if(ss.length==3)
			{
				tuples.add(buffer);
			}
		}
		System.out.println(tuples.size());
		return tuples;
	}
	
	
	public List<String> MergeIntersection(List<String> l1,List<String> l2)
	{
		Set<String> s1=new HashSet<String>();
		s1.addAll(l1);
		Set<String> s2=new HashSet<String>();
		s2.addAll(l2);
		
		s1.retainAll(s2);
		List<String> r=new ArrayList<String>();
		r.addAll(s1);
		System.out.println(r.size());
		return r;
	}
	
	public List<String> MergeUnion(List<String> l1,List<String> l2)
	{
		Set<String> s1=new HashSet<String>();
		s1.addAll(l1);
		Set<String> s2=new HashSet<String>();
		s2.addAll(l2);
		
		s1.addAll(s2);
		List<String> r=new ArrayList<String>();
		r.addAll(s1);
		
		System.out.println(r.size());
		return r;
	}	
	
	public void WriteJava(String filename,List<String> list) throws FileNotFoundException
	{
		PrintStream ps=new PrintStream(filename);
		for(int i=0;i<list.size();i++)
		{
			ps.println(list.get(i));
		}
		ps.close();
	}
	public void WriteJava2(String filename,List<String> list) throws FileNotFoundException
	{
		PrintStream ps=new PrintStream(filename);
		int count=0;
		for(int i=0;i<list.size();i++)
		{
			if(i%1000==0)
			{
				ps.println("};");
				ps.println("}");
				ps.println("static void init"+count+"() {");
				ps.println("a"+count+" = new int[] {");
				count++;
			}
			ps.println(list.get(i));
		}
		ps.close();
	}	
	public static void main(String[] args) throws IOException
	{
		MergeJava mj=new MergeJava();
		//was best
		//List<String> l1=mj.ReadJavaFile("C:\\Users\\Administrator\\Desktop\\tc_data\\exper6\\BannerAnnotate.java");
		//List<String> l2=mj.ReadJavaFile("F:\\Workspace\\BannerAnnotate\\crowd_words\\lines");
		
		List<String> l1=mj.ReadJavaFile("F:\\Workspace\\Banner\\banner_source\\BannerAnnotate.java");
		List<String> l2=mj.ReadJavaFile("C:\\Users\\Administrator\\Desktop\\tc_data\\svm2\\Heightest.java");		
		
		
		
		List<String> l3=mj.MergeIntersection(l1, l2);
		mj.WriteJava2("lines", l3);
		System.out.println(l3.size()*3);
	}
	
}
