package wzy;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.stream.XMLStreamException;

import org.scripps.crowdwords.Annotation;
import org.scripps.crowdwords.Document;
import org.scripps.crowdwords.TestAggregation;

import wzy.model.AnnoAndCount;

import bioc.BioCCollection;

public class FilterTwoSentencesMention {
	
	public List<Annotation> annosList;
	public Map<String, Document> id_doc;
	
	public void Filter(String filename) throws XMLStreamException, IOException
	{
		BioCCollection mturk_collection = TestAggregation.readBioC(filename);
		id_doc = TestAggregation.convertBioCCollectionToDocMap(mturk_collection);
		annosList = TestAggregation.convertBioCtoAnnotationList(mturk_collection);
		
		List<Annotation> filtedList=new ArrayList<Annotation>();
		
		for(int i=0;i<annosList.size();i++)
		{
			Annotation an=annosList.get(i);
			String text=an.getText();
			if(text.indexOf('.')>=0||text.indexOf('?')>=0||text.indexOf('!')>=0||text.indexOf(';')>=0)
				continue;
			filtedList.add(an);
		}
		
		BioCCollection k_collection = TestAggregation.convertAnnotationsToBioC(filtedList,id_doc,"svm_filted","",10000000);
		TestAggregation.writeBioC(k_collection,"F:\\Workspace\\Banner\\banner_source\\data\\only_svm_filter2.xml");
	}
	
	public static void main(String[] args) throws XMLStreamException, IOException
	{
		FilterTwoSentencesMention f=new FilterTwoSentencesMention();
		f.Filter("F:\\Workspace\\Banner\\banner_source\\data\\svm2.xml");
	}
	
}
