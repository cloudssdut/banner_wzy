package wzy;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.stream.XMLStreamException;

import org.scripps.crowdwords.Annotation;
import org.scripps.crowdwords.Document;
import org.scripps.crowdwords.TestAggregation;

import wzy.model.AnnoAndCount;
import wzy.model.SVMDataPoint;

import bioc.BioCCollection;

public class WorkerDataProcess {

	public Map<Integer,Set<Integer>> annoToUserSet=new HashMap<Integer,Set<Integer>>();
	public Map<Integer,Annotation> idToAnno=new HashMap<Integer,Annotation>();
	public Map<String,Integer> textToId=new HashMap<String,Integer>();
	public List<String> idToText=new ArrayList<String>();
	public Set<Integer> annoSet=new HashSet<Integer>();
	public List<Annotation> annosList;
	public List<AnnoAndCount> annoAndCountList=new ArrayList<AnnoAndCount>();
	public Map<String, Document> id_doc;
	public List<Integer> labelList=new ArrayList<Integer>();
	
	public void ReadBiocFile(String filename) throws FileNotFoundException, XMLStreamException
	{
		BioCCollection mturk_collection = TestAggregation.readBioC(filename);
		id_doc = TestAggregation.convertBioCCollectionToDocMap(mturk_collection);
		annosList = TestAggregation.convertBioCtoAnnotationList(mturk_collection);
		for(int i=0;i<annosList.size();i++)
		{
			Annotation an=annosList.get(i);
			
			Integer id=textToId.get(an.getText());
			if(id==null)
			{
				id=textToId.size();
				textToId.put(an.getText(), id);
				idToText.add(an.getText());
			}
			
			idToAnno.put(id, an);
			annoSet.add(id);
			Set<Integer> userSet=annoToUserSet.get(id);
			if(userSet==null)
			{
				userSet=new HashSet<Integer>();
				annoToUserSet.put(id, userSet);
			}
			userSet.add(an.getUser_id());
		}
		//annoAndCountList.clear();
		Iterator it=annoToUserSet.entrySet().iterator();
		while(it.hasNext())
		{
			Map.Entry entry=(Map.Entry)it.next();
			Integer id=(Integer)entry.getKey();
			Set<Integer> userSet=(Set<Integer>)entry.getValue();
			AnnoAndCount aac=new AnnoAndCount();
			aac.id=id;
			aac.count=userSet.size();
			annoAndCountList.add(aac);
		}
		
		Collections.sort(annoAndCountList, new AnnoAndCount());
		System.out.println("Missing: "+annoAndCountList.size()+"\t"+annoSet.size()+"\t"+annoToUserSet.size());
	}
	
	
	public static void CalculateAR(WorkerDataProcess expert,WorkerDataProcess work)
	{
		Set<Integer> accSet=new HashSet<Integer>();
		accSet.addAll(work.annoSet);
		accSet.retainAll(expert.annoSet);
		System.out.println(accSet.size()+"\t"+work.annoSet.size());
		System.out.println(accSet.size()+"\t"+expert.annoSet.size());
	}
	
	public static List<SVMDataPoint> ProduceData(WorkerDataProcess expert,WorkerDataProcess work)
	{
		Iterator it=work.annoSet.iterator();
		int maxId=-1;
		int minId=999999;
		List<SVMDataPoint> sdpList=new ArrayList<SVMDataPoint>();
		while(it.hasNext())
		{
			Integer anid_work=(Integer)it.next();
			SVMDataPoint sdp=new SVMDataPoint();
			sdp.id=anid_work;
			if(expert!=null)
				if(expert.annoSet.contains(anid_work))
				{
					sdp.lable=1;
				}
				else
				{
					sdp.lable=-1;
				}
			else
				sdp.lable=1;
			
			Set<Integer> userSet=work.annoToUserSet.get(anid_work);
			List<Integer> userList=new ArrayList<Integer>();
			userList.addAll(userSet);
			Collections.sort(userList);
			sdp.featureList=userList;
			for(int i=0;i<sdp.featureList.size();i++)
			{
				sdp.valueList.add(1.0);
			}
			if(sdp.featureList.size()>0)
				sdpList.add(sdp);
		}
		return sdpList;
	}
	
	public static void WriteSVMdpList(List<SVMDataPoint> sdpList,String filename,String indexfile) throws FileNotFoundException
	{
		PrintStream ps=new PrintStream(filename);
		PrintStream psindex=new PrintStream(indexfile);
		for(int i=0;i<sdpList.size();i++)
		{
			ps.print(sdpList.get(i).lable+" ");
			int j=0;
			for(;j<sdpList.get(i).featureList.size()-1;j++)
			{
				ps.print(sdpList.get(i).featureList.get(j)+":"+sdpList.get(i).valueList.get(j)+" ");
			}
			ps.println(sdpList.get(i).featureList.get(j)+":"+sdpList.get(i).valueList.get(j));
		}
		for(int i=0;i<sdpList.size();i++)
		{
			psindex.println(sdpList.get(i).id);
		}
		ps.close();
	}
	
	public void ReadLabels(String filename) throws IOException
	{
		BufferedReader br=new BufferedReader(new FileReader(filename));
		String buffer=null;
		while((buffer=br.readLine())!=null)
		{
			if(buffer.length()<1)
				continue;
			Integer label=Integer.parseInt(buffer);
			labelList.add(label);
		}
	}
	public static void WriteNewInstances() throws IOException, XMLStreamException
	{
		WorkerDataProcess worker2=new WorkerDataProcess();
		worker2.ReadLabels("F:\\Workspace\\Banner\\banner_source\\data\\banner_tt\\svm2.out");

		worker2.ReadBiocFile("F:\\Workspace\\Banner\\banner_source\\data\\mturk2_bioc.xml");		
		List<SVMDataPoint> sdpList2=ProduceData(null,worker2);
		
		if(worker2.labelList.size()!=sdpList2.size())
		{
			System.out.println("ERROR: different size of svmdata");
			System.exit(-1);
		}
		
		List<Annotation> annList=new ArrayList<Annotation>();
		for(int i=0;i<worker2.labelList.size();i++)
		{
			if(worker2.labelList.get(i)>0)
			{
				Annotation an=worker2.idToAnno.get(sdpList2.get(i).id);
				annList.add(an);
			}
		}

		BioCCollection k_collection = TestAggregation.convertAnnotationsToBioC(annList, worker2.id_doc,"svm","",10000000);
		TestAggregation.writeBioC(k_collection,"F:\\Workspace\\Banner\\banner_source\\data\\svm2.xml");
	}
	
	public static void main(String[] args) throws XMLStreamException, IOException
	{
/*		WorkerDataProcess worker=new WorkerDataProcess();
		worker.ReadBiocFile("F:\\Workspace\\Banner\\banner_source\\data\\mturk1_bioc.xml");
		
		WorkerDataProcess expert=new WorkerDataProcess();
		expert.ReadBiocFile("F:\\Workspace\\Banner\\banner_source\\data\\expert1_bioc.xml");
		
		//CalculateAR(expert,worker);
		List<SVMDataPoint> sdpList=ProduceData(expert,worker);
		WriteSVMdpList(sdpList,"F:\\Workspace\\Banner\\banner_source\\data\\svm.train"
				,"F:\\Workspace\\Banner\\banner_source\\data\\svm.train.index");
		
		WorkerDataProcess worker2=new WorkerDataProcess();
		worker2.ReadBiocFile("F:\\Workspace\\Banner\\banner_source\\data\\mturk2_bioc.xml");		
		
		List<SVMDataPoint> sdpList2=ProduceData(null,worker2);
		WriteSVMdpList(sdpList2,"F:\\Workspace\\Banner\\banner_source\\data\\svm.test"
				,"F:\\Workspace\\Banner\\banner_source\\data\\svm.test.index");		*/
		
		WriteNewInstances();
		
	}
}
