package wzy;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.xml.stream.XMLStreamException;

import org.scripps.crowdwords.Annotation;
import org.scripps.crowdwords.Document;
import org.scripps.crowdwords.TestAggregation;


import bioc.BioCCollection;

public class SampleTurnData {

	
	public List<Annotation> annoList=new ArrayList<Annotation>();
	public Map<String, Document> id_doc=new HashMap<String,Document>();
	
	public void ReadFile(String filename) throws FileNotFoundException, XMLStreamException
	{
		BioCCollection mturk_collection = TestAggregation.readBioC(filename);
		id_doc = TestAggregation.convertBioCCollectionToDocMap(mturk_collection);
		annoList = TestAggregation.convertBioCtoAnnotationList(mturk_collection);
	}
	
	public void AppendReadFile(String filename) throws FileNotFoundException, XMLStreamException  //id�Ǹ�����
	{
		BioCCollection mturk_collection = TestAggregation.readBioC(filename);
		id_doc.putAll(TestAggregation.convertBioCCollectionToDocMap(mturk_collection));
		annoList.addAll(TestAggregation.convertBioCtoAnnotationList(mturk_collection));
	}	
	
	public void SampleAndWrite(String dir,int M,int N) throws XMLStreamException, IOException
	{
		Random rand=new Random();
		for(int i=0;i<M;i++)
		{
			List<Annotation> subList=new ArrayList<Annotation>();
			for(int j=0;j<N;j++)
			{
				int index=Math.abs(rand.nextInt())%annoList.size();
				subList.add(annoList.get(index));
			}
			BioCCollection k_collection = TestAggregation.convertAnnotationsToBioC(subList, id_doc,"sample"+i,"",1000000);
			TestAggregation.writeBioC(k_collection, dir+"sample"+i+".xml");
		}
	}
	
	public List<List<Annotation>> OnlySample(int M,int N)
	{
		List<List<Annotation>> annoLL=new ArrayList<List<Annotation>>();
		Random rand=new Random();
		for(int i=0;i<M;i++)
		{
			List<Annotation> subList=new ArrayList<Annotation>();
			for(int j=0;j<N;j++)
			{
				int index=Math.abs(rand.nextInt())%annoList.size();
				subList.add(annoList.get(index));
			}
			//BioCCollection k_collection = TestAggregation.convertAnnotationsToBioC(subList, id_doc,"sample"+i,"",1000000);
			//TestAggregation.writeBioC(k_collection, dir+"sample"+i+".xml");
			
			annoLL.add(subList);
		}
		return annoLL;
	}
	
	public static void main(String args[]) throws XMLStreamException, IOException
	{
		SampleTurnData s=new SampleTurnData();
		s.AppendReadFile("C:\\Users\\Administrator\\Desktop\\tc_data\\exper6\\data\\erper6.xml");
		List<List<Annotation>> randLL=s.OnlySample(10, (int)(s.annoList.size()*0.5));
		s.annoList.clear();
		s.AppendReadFile("F:\\Workspace\\Banner\\banner_source\\data\\expert1_bioc.xml");
		String dir="C:\\Users\\Administrator\\Desktop\\tc_data\\sample\\";
		for(int i=0;i<randLL.size();i++)
		{
			List<Annotation> annoList=new ArrayList<Annotation>();
			annoList.addAll(randLL.get(i));
			annoList.addAll(s.annoList);
			BioCCollection k_collection = TestAggregation.convertAnnotationsToBioC(annoList, s.id_doc,"sample"+i,"",1000000);
			TestAggregation.writeBioC(k_collection, dir+"sample"+i+".xml");	
		}
	}
}
