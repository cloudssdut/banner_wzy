package wzy;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.stream.XMLStreamException;

import org.scripps.crowdwords.Annotation;
import org.scripps.crowdwords.TestAggregation;

import bioc.BioCCollection;

public class CountLines {

	public List<List<String>> javaList=new ArrayList<List<String>>();
	public Map<String,Integer> lineCounts=new HashMap<String,Integer>();
	public List<String> resultList=new ArrayList<String>();
	
	public void WriteJava2(String filename,List<String> list) throws FileNotFoundException
	{
		PrintStream ps=new PrintStream(filename);
		int count=0;
		for(int i=0;i<list.size();i++)
		{
			if(i%1000==0)
			{
				ps.println("};");
				ps.println("}");
				ps.println("static void init"+count+"() {");
				ps.println("a"+count+" = new int[] {");
				count++;
			}
			ps.println(list.get(i));
		}
		ps.close();
	}	
	public List<String> ReadJavaFile(String filename) throws IOException
	{
		List<String> tuples=new ArrayList<String>();
		BufferedReader br=new BufferedReader(new FileReader(filename));
		String buffer=null;
		while((buffer=br.readLine())!=null)
		{
			if(buffer.length()<2)
				continue;
			String[] ss=buffer.split(",");
			//System.out.println("*"+ss.length);
			if(ss.length==3)
			{
				tuples.add(buffer);
			}
		}
		System.out.println(tuples.size());
		return tuples;
	}
	
	public void CountTuples()
	{
		for(int i=0;i<javaList.size();i++)
		{
			for(int j=0;j<javaList.get(i).size();j++)
			{
				Integer count=lineCounts.get(javaList.get(i).get(j));
				if(count==null)
				{
					lineCounts.put(javaList.get(i).get(j), 1);
				}
				else
				{
					count++;
					lineCounts.remove(javaList.get(i).get(j));
					lineCounts.put(javaList.get(i).get(j), count);
				}
			}
		}
	}
	
	public void GetTopatK(int K)
	{
		Iterator it=lineCounts.entrySet().iterator();
		while(it.hasNext())
		{
			Map.Entry entry=(Map.Entry)it.next();
			String tuple=(String)entry.getKey();
			Integer count=(Integer)entry.getValue();
			if(count>=K)
			{
				resultList.add(tuple);
			}
		}
	}
	
	
	public static void MergeK(String[] filenames,int K,String outputfile) throws IOException
	{
		CountLines cl=new CountLines();
		for(int i=0;i<filenames.length;i++)
		{
			cl.javaList.add(cl.ReadJavaFile(filenames[i]));
		}
		cl.CountTuples();
		cl.GetTopatK(K);
		System.out.println(cl.resultList.size()+"\t"+cl.resultList.size()*3);
		cl.WriteJava2(outputfile, cl.resultList);
	}
	
	public static void main(String[] args) throws IOException
	{
/*		String[] files={"C:\\Users\\Administrator\\Desktop\\tc_data\\exper6\\BannerAnnotate.java",
				"F:\\Workspace\\Banner\\banner_source\\outputĬ������\\BannerAnnotate.java",
				"C:\\Users\\Administrator\\Desktop\\tc_data\\agree10\\BannerAnnotate.java"};*/
		String[] files={"C:\\Users\\Administrator\\Desktop\\submision\\10.java",
				"C:\\Users\\Administrator\\Desktop\\submision\\11.java",
				"C:\\Users\\Administrator\\Desktop\\submision\\12.java",
				"C:\\Users\\Administrator\\Desktop\\submision\\13.java",
				"C:\\Users\\Administrator\\Desktop\\submision\\14.java",
				"C:\\Users\\Administrator\\Desktop\\submision\\15.java",
				"C:\\Users\\Administrator\\Desktop\\submision\\16.java"};
		
		
		MergeK(files,3,"lines");
	}
	
}
