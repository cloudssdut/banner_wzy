package wzy.model;

import java.util.Comparator;

public class AnnoAndCount implements Comparator{

	public int id;
	public int count;
	@Override
	public int compare(Object o1, Object o2) {
		// TODO Auto-generated method stub
		AnnoAndCount a1=(AnnoAndCount)o1;
		AnnoAndCount a2=(AnnoAndCount)o2;
		if(a1.count==a2.count)
			return 0;
		if(a1.count>a2.count)
			return -1;
		else
			return 1;
	}
	
}
