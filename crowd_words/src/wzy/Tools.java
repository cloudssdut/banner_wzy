package wzy;

import java.util.ArrayList;
import java.util.List;

import org.scripps.crowdwords.Annotation;

public class Tools {

	public static boolean IsHaveDot(Annotation anno)
	{
		return anno.getText().indexOf(".")>=0||anno.getText().indexOf("?")>=0||anno.getText().indexOf("!")>=0||anno.getText().indexOf(";")>=0;
	}
	
	public static List<Annotation> FilterAllTextHaveDot(List<Annotation> annoList)
	{
		List<Annotation> resultList=new ArrayList<Annotation>();
		for(int i=0;i<annoList.size();i++)
		{
			if(IsHaveDot(annoList.get(i)))
			{
				continue;
			}
			resultList.add(annoList.get(i));
		}
		return resultList;
	}
	
}
