package wzy;

import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.xml.stream.XMLStreamException;

import org.scripps.crowdwords.Annotation;
import org.scripps.crowdwords.TestAggregation;

import bioc.BioCCollection;

public class ProduceDict {

	public List<String> dictList=new ArrayList<String>();
	
	
	
	public void ProduceDict(String filename) throws FileNotFoundException, XMLStreamException
	{
		BioCCollection mturk_collection = TestAggregation.readBioC(filename);
		List<Annotation> mturk_annos = TestAggregation.convertBioCtoAnnotationList(mturk_collection);
		for(int i=0;i<mturk_annos.size();i++)
		{
			dictList.add(mturk_annos.get(i).getText());
		}
	}
	
	public void FilterDup()
	{
		Set<String> set=new HashSet<String>();
		set.addAll(dictList);
		dictList=new ArrayList<String>();
		dictList.addAll(set);
	}
	
	public void WriteDict(String filename) throws FileNotFoundException
	{
		PrintStream ps=new PrintStream(filename);
		for(int i=0;i<dictList.size();i++)
		{
			ps.println(dictList.get(i));
		}
	}
	
	public static void main(String[] args) throws FileNotFoundException, XMLStreamException
	{
		ProduceDict pd=new ProduceDict();
		pd.ProduceDict("F:\\Workspace\\Banner\\banner_source\\data\\expert1_bioc.xml");
		pd.ProduceDict("F:\\Workspace\\Banner\\banner_source\\data\\mturk2_bioc.xml");		
		pd.FilterDup();
		pd.WriteDict("F:\\Workspace\\Banner\\banner_source\\dict\\desease3.dict");
	}
	
}
